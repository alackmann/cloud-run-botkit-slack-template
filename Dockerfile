FROM node:10-alpine

RUN mkdir /opt/bot
WORKDIR /opt/bot
ADD . /opt/bot

RUN npm install

EXPOSE 3000

CMD [ "node", "bot.js"]