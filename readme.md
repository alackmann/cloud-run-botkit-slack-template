# Cloud Run Botkit Slack Quickstart template
A template repository for creating Slack bots using the [botkit](https://botkit.ai) framework, hosted on the [Google Cloud Run](https://cloud.google.com/run/) serverless HTTP containers PaaS, including scripted build, push and deploy.

# Botkit
This is a Botkit starter kit for slack, created with the [Yeoman generator](https://github.com/howdyai/botkit/tree/master/packages/generator-botkit#readme). For more information on using Botkit, see the [Botkit Docs](https://botkit.ai/docs/v4).


# Getting started
To get started, clone this repository locally and start setting up your dependencies. You're going to need:

* Docker installed locally
* admin access to a Slack instance
* an account with ngrok for testing locally before deploying on Google Cloud
* a Google Cloud account

## Docker build
This template is configured to simply echo back to the poster anything said in a channel the bot is added to. This is controlled in `features/sample_echo.js`. 

To build your initial docker container, run `docker build -t gcpbot:latest .` (change the tag as you see fit).

## Slack configuration
1. Got to the [Slack Apps configuration page](https://api.slack.com/apps) and `Create a New App`.
1. On your new app, go to the `Bot Users` page and configure a new bot and click `Add Bot User`.
1. Go to the `Install App` page and click `Install app` and authorise your bot on your Slack instance.
1. You should now have access to the `Bot User OAuth Access Token`, copy this token. Do the same for the `Signing secret` from the `Basic Information` page.

## Run docker container locally
Stand up your local docker container using the slack credentials you've generated in the step above.

```
docker run -p 3000:3000 -e clientSigningSecret=<your-signing-secret> -e botToken=<your-bot-token> -t gcpbot:latest
```

## Configure ngrok and update Slack
To enable internet traffic from Slack to reach your local machine, create a tunnel using [ngrok](https://ngrok.com) (if you've not already done so, signup an account first and install ngrok). Then run `./ngrok http 3000` to setup a URL for your slack bot.

Ngrok will provide you an external URL like https://abc123xyz.ngrok.io and locally you should be able to visit http://localhost:4040 to view stats of your tunnel (handy for testing)

Copy this URL and return to your Slack App config in your browser.

1. Go to the `OAuth and Permissions` page and update the Redirect URLs to be `https://[your-ngrok].ngrok.io/install`
1. On the `Event Subscriptions` page, update the `Request URL` field to be `https://[your-ngrok].ngrok.io/api/messages`
1. On the same page, add a subscription to `message.channels` - this sends all messages from any channels this bot is installed in to your bot for handling.
1. On a slack channeln in your Workspace, add your bot using `/invite <your-bot_name>`. You should be able to see traffic at you ngrok web interface.
1. Test by typing a message into the channel. It should respond by repeating your text back to you.

# Deploy to Production on Google Cloud Run
## Setup
If you haven't already, [install the gcloud command line](https://cloud.google.com/sdk/install) tool and complete the [standard authentication and project setup](https://cloud.google.com/sdk/docs/initializing).

Once you have configured Project you plan to deploy the bot into, enable _Cloud Build_ and _Cloud Run_ and (optionally) if you want simple persistence for your bot _Cloud Datastore_
```
gcloud services enable cloudbuild.googleapis.com
gcloud services enable run.googleapis.com
gcloud services enable datastore.googleapis.com
```

You'll need to [setup some permissions](https://cloud.google.com/run/docs/continuous-deployment) to allow Cloud Build to run a `deploy` on the _Run_ service.

## Configure build
Edit the `cloudbuild.yml` file in the root directory, replacing the `clientSigningSecret` and `botToken` values in the last line. The run:
```
gcloud builds submit --config cloudbuild.yaml .
```

This should trigger a remote build of your Docker container and push it to _Cloud Run_ and give you a published HTTPS endpoint.

## Update Slack
Return to your Slack App config page and on the `OAuth and Permissions` and `Event Subscriptions` pages, replace the _ngrok_ domains with the newly published _Cloud Run_ ones. Retest from Slack.


# TODO:
* Move `sigingSecret` and `botToken` to KMS secrets
* Configure triggered builds from your repository to complete a CI/CD process



